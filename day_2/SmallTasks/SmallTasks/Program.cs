﻿namespace SmallTasks
{
    public class Program
    {
        public static void Main()
        {
            int[] array = { 3, 4, 6, 8 };

            Console.WriteLine(MinElementInArray(array));
            Console.ReadLine();
        }

        public static int IntLimit (int num)
        {
            num = Int32.MaxValue;
            num += 1;

            return num;
        }

        public static bool IsOdd (int num)
        {
            return num % 2 != 0;
        }

        public static bool IsPrime (int num)
        {
            int i;
            for (i = 2; i <= num - 1; i++)
            {

                if (num % i == 0)
                {
                    return false;
                }
            }

            if (i == num)
            {
                return true;
            }

            return false;
        }

        public static int MinElementInArray(int[] arr)
        {
            int minElement = Int32.MaxValue;

            for (int i = 0; i < arr.Length; i++)
            {

                if (arr[i] < minElement)
                {
                    minElement = arr[i];
                }
            }

            return minElement;
        }
    }
}