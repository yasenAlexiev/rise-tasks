using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System;

namespace MissingNumberTests
{
    [TestClass]
    public class MissingNumberTest
    {
        [TestMethod]
        public void TestMethodArrayWithNineNumbers()
        {
            //Arrange
            int[] input = { 6, 11, 3, 4, 9, 7, 5, 2, 8 };
            int expected = 10;

            //Act
            int actual = MissingNumber.FindMissingNumber.FindMissingNumberFn(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "The input array must contain atleast 2 numbers"
            )]
        public void TestMethodArrayWithOneNumber()
        {
            //Arrange
            int[] input = { 6 };

            //Act
            try
            {
                MissingNumber.FindMissingNumber.FindMissingNumberFn(input);
            }

            catch (ArgumentException ex)
            {
            //Assert
            Assert.AreEqual("The input array must contain atleast 2 numbers", ex.Message);
                throw;
            }

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "There are negative numbers in the array"
            )]
        public void TestMethodArrayWithNegativeNumbers()
        {
            //Arrange
            int[] input = { -6, -4 };

            //Act
            try
            {
                MissingNumber.FindMissingNumber.FindMissingNumberFn(input);
            }

            catch (ArgumentException ex)
            {
                //Assert
                Assert.AreEqual("There are negative numbers in the array", ex.Message);
                throw;
            }

        }
    }
}