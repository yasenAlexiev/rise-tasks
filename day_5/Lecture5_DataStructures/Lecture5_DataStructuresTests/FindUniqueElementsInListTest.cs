using Lecture5_DataStructures;

namespace Lecture5_DataStructuresTests
{
    [TestClass]
    public class FindUniqueElementsInListTest
    {
        [TestMethod]
        public void TestMethodFindUniqueElementsInListWith5Elements()
        {
            // Arrange
            UniqueElements uniqueElements = new UniqueElements();
            List<int> list = new List<int>() { 1, 2, 2, 3, 4, 4, 7, 7 };

            List<int> expected = new List<int>() { 1, 2, 3, 4, 7 };

            // Act
            List<int> actual = new List<int>(uniqueElements.findUniqueElementsInList(list));

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodFindUniqueElementsInListWith0Elements()
        {
            // Arrange
            UniqueElements uniqueElements = new UniqueElements();
            List<int> list = new List<int>();

            List<int> expected = new List<int>();

            // Act
            List<int> actual = new List<int>(uniqueElements.findUniqueElementsInList(list));

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodFindUniqueElementsInListWith5NegativeElements()
        {
            // Arrange
            UniqueElements uniqueElements = new UniqueElements();
            List<int> list = new List<int>() { -1, -2, -2, -3, -4, -4, -7, -7 };

            List<int> expected = new List<int>() { -1, -2, -3, -4, -7 };

            // Act
            List<int> actual = new List<int>(uniqueElements.findUniqueElementsInList(list));

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodFindUniqueElementsInListWith4NegativeAnd3PositiveElements()
        {
            // Arrange
            UniqueElements uniqueElements = new UniqueElements();
            List<int> list = new List<int>() { -1, -1, -2, 2, -3, 4, 4, 7, -7 };

            List<int> expected = new List<int>() { -1, -2, 2, -3, 4, 7, -7 };

            // Act
            List<int> actual = new List<int>(uniqueElements.findUniqueElementsInList(list));

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}