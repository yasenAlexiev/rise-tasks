﻿using Lecture5_DataStructures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_DataStructuresTests
{
    [TestClass]
    public class RemoveMiddleElementOfLinkedListTest
    {
        [TestMethod]
        public void TestMethodRemoveMiddleElementOfLinkedListWithOddNumberOfElements()
        {
            // Arrange
            MiddleElementsOfLinkedList middleElements = new MiddleElementsOfLinkedList();
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.AddLast(1);
            linkedList.AddLast(2);
            linkedList.AddLast(3);
            linkedList.AddLast(4);
            linkedList.AddLast(5);

            LinkedList<int> expected = new LinkedList<int>();
            expected.AddLast(1);
            expected.AddLast(2);
            expected.AddLast(4);
            expected.AddLast(5);

            // Act
            LinkedList<int> actual = new LinkedList<int>(middleElements.RemoveMiddleElementOfLinkedList(linkedList));
            
            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodRemoveMiddleElementOfLinkedListWithEvenNumberOfElements()
        {
            // Arrange
            MiddleElementsOfLinkedList middleElements = new MiddleElementsOfLinkedList();
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.AddLast(1);
            linkedList.AddLast(2);
            linkedList.AddLast(3);
            linkedList.AddLast(4);

            LinkedList<int> expected = new LinkedList<int>();
            expected.AddLast(1);
            expected.AddLast(4);

            // Act
            LinkedList<int> actual = new LinkedList<int>(middleElements.RemoveMiddleElementOfLinkedList(linkedList));

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodRemoveMiddleElementOfLinkedListWith1Element()
        {
            // Arrange
            MiddleElementsOfLinkedList middleElements = new MiddleElementsOfLinkedList();
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.AddLast(1);

            LinkedList<int> expected = new LinkedList<int>();

            // Act
            LinkedList<int> actual = new LinkedList<int>(middleElements.RemoveMiddleElementOfLinkedList(linkedList));

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "The input LinkedList must contain atleast 1 element"
            )]
        public void TestMethodRemoveMiddleElementOfLinkedListWith0Elements()
        {
            // Arrange
            MiddleElementsOfLinkedList middleElements = new MiddleElementsOfLinkedList();
            LinkedList<int> linkedList = new LinkedList<int>();

            // Act
            try
            {
                middleElements.RemoveMiddleElementOfLinkedList(linkedList);
            }

            catch (ArgumentException ex)
            {
                //Assert
                Assert.AreEqual("The input LinkedList must contain atleast 1 element", ex.Message);
                throw;
            }
        }
    }
}
