﻿namespace Lecture5_DataStructures
{
    public class Program
    {
        public static void Main()
        {
            List<int> list = new List<int>() { -1, -2, 2, -3, 4, 4, 7, -7 };
            UniqueElements uniqueElements = new UniqueElements();

            foreach (var element in uniqueElements.findUniqueElementsInList(list))
            {
                Console.WriteLine(element);
            }
            
            LinkedList<int> list2 = new LinkedList<int>();
            list2.AddLast(1);
            list2.AddLast(2);
            list2.AddLast(3);
            list2.AddLast(4);
            list2.AddLast(5);
            list2.AddLast(6);
            list2.AddLast(7);
            list2.AddLast(8);
            list2.AddLast(9);
            list2.AddLast(10);
            list2.AddLast(11);
            list2.AddLast(12);
            list2.AddLast(13);
            list2.AddLast(14);
            list2.AddLast(15);
            list2.AddLast(16);
            list2.AddLast(17);
            list2.AddLast(18);
            list2.AddLast(19);

            MiddleElementsOfLinkedList middleElement = new MiddleElementsOfLinkedList();
            middleElement.RemoveMiddleElementOfLinkedList(list2);
            Console.WriteLine();
        }
    }
}