﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_DataStructures
{
    public class MiddleElementsOfLinkedList
    {
        public LinkedList<int> RemoveMiddleElementOfLinkedList(LinkedList<int> linkedList)
        {

            if (linkedList.Count == 0)
            {
                throw new ArgumentException("The input LinkedList must contain atleast 1 element");
            }

            if (linkedList.Count % 2 == 0)
            {
                int positionOfMiddleElementInLinkedList = linkedList.Count / 2;
                int positionOfSecondMiddleElementInLinkedList = linkedList.Count / 2 - 1;

                linkedList.Remove(linkedList.ElementAt(positionOfMiddleElementInLinkedList));
                linkedList.Remove(linkedList.ElementAt(positionOfSecondMiddleElementInLinkedList));

                return linkedList;
            }

            else
            {
                int positionOfMiddleElementInLinkedList = linkedList.Count / 2;

                linkedList.Remove(linkedList.ElementAt(positionOfMiddleElementInLinkedList));

                return linkedList;
            }
        }
    }
}
