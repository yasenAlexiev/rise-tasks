﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingExercises
{
    public class IntersectionOfTwoArrays
    {
        public static string[] FindIntersectionOfTwoArrays(string[] firstArrayOfElements, string[] secondArrayOfElements ) { // вярно
            HashSet<string> firstSet = new HashSet<string>(firstArrayOfElements);
            HashSet<string> secondSet = new HashSet<string>(secondArrayOfElements);

            HashSet<string> intersected = new HashSet<string>(firstSet); // можеше веднага да направиш резултатния HashSet от firstArrayOfElements, а не да правиш HashSet-a на 13 ред
            intersected.Intersect(secondSet);

            string[] intersectedArray = new string[intersected.Count];
            intersected.CopyTo(intersectedArray);

            return intersectedArray;
        }
    }
}
