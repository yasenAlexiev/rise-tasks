﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TreesExercises.Model
{
    public class PreorderTraversal
    {
        public static void PrintNodesInPreorderTraversal(BinaryTreeNode binaryTree)
        {

            if (binaryTree == null)
            {
                return;
            }

            Console.Write(binaryTree.Value + " ");

            PrintNodesInPreorderTraversal(binaryTree.Left);
            PrintNodesInPreorderTraversal(binaryTree.Right);
        }
    }
}
