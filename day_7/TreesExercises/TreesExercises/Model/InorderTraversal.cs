﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreesExercises.Model
{
    public class InorderTraversal
    {
        public static void PrintNodesInInorderTraversal(BinaryTreeNode binaryTree)
        {

            if (binaryTree == null)
            {
                return;
            }

            PrintNodesInInorderTraversal(binaryTree.Left);

            Console.Write(binaryTree.Value + " ");

            PrintNodesInInorderTraversal(binaryTree.Right);
        }
    }
}
