﻿using TreesExercises.Model;

namespace TreesExercises
{
    public class Program
    {
        public static void Main()
        {
            var node61 = new BinaryTreeNode(61, null, null);
            var node76 = new BinaryTreeNode(76, null, null);

            var node35 = new BinaryTreeNode(40, null, null);
            var node24 = new BinaryTreeNode(24, null, null);

            var node38 = new BinaryTreeNode(38, node35, null);
            var node69 = new BinaryTreeNode(69, node61, node76);

            var node52 = new BinaryTreeNode(52, node38, node69);
            var node11 = new BinaryTreeNode(11, null, node24);

            var binaryTree = new BinaryTreeNode(25, node11, node52);

            PreorderTraversal.PrintNodesInPreorderTraversal(binaryTree);

            Console.WriteLine();

            PostorderTraversal.PrintNodesInPostorderTraversal(binaryTree);

            Console.WriteLine();

            InorderTraversal.PrintNodesInInorderTraversal(binaryTree);

            Console.WriteLine();

            Console.WriteLine(NodeHeight.FindNodeHeight(binaryTree));

            Console.WriteLine();
            Console.ReadLine();
        }
    }
}