using GraphExercises;

namespace GraphExercisesTests
{
    [TestClass]
    public class TestIsCyclicMethod
    {
        [TestMethod]
        public void TestMethodIsCyclicWithCyclicGraph()
        {
            // Arrange
            Graph graph = new Graph(3);

            graph.AddEdge(0, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 0);

            bool expected = true;

            // Act
            bool actual = graph.isCyclic();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodIsCyclicWithNonCyclicGraph()
        {
            // Arrange
            Graph graph = new Graph(4);

            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(2, 3);
            graph.AddEdge(1, 2);

            bool expected = false;

            // Act
            bool actual = graph.isCyclic();

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}