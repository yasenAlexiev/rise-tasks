﻿using OOPNatureReserveSimulationSolution.Animals;

namespace OOPNatureReserveSimulationSolution
{
    interface IAnimal : IFood
    {
        void Feed(IFood foodItem, List<Animal> listOfAnimals);
        void GetEaten(IFood foodItem);
        void Starves(IFood foodItem);
        void Dies(IFood foodItem, List<Animal> listOfAnimals);
    }
}
