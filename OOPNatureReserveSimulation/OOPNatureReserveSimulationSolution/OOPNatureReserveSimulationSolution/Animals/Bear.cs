﻿namespace OOPNatureReserveSimulationSolution.Animals;

public class Bear : Animal
{
    public Bear() : base("bear", 10, 20, new HashSet<string> { "Beaver", "Hare", "Fruit", "Wolf" }, "Omnivore")
    {
    }
}