﻿namespace OOPNatureReserveSimulationSolution.Animals;

public class AnimalLifeSpan
{
    public static Statistics SimulateLifeSpan()
    {
        var animals = CreateAnimals();
        var foods = CreateFoods();

        int animalsCount = animals.Count();

        int minLifeSpanOfAnimals = int.MaxValue;
        int maxLifeSpanOfAnimals = int.MinValue;
        int averageLifeSpanOfAnimals = 0;

        var currentStatistics = new int[3];

        int turns = 0;

        foods = AddAnimalsToFoods(animals, foods);

        while (animals.Count > 0)
        {
            var randomFood = RandomFoodGenerator(foods);
            turns++;

            foreach (Animal animal in animals.ToList())
            {
                //var animal = ChangeDietIfAnimalCertainAgeAndType(_animal);
                animal.Feed(randomFood, animals);
                currentStatistics = animal.UpdateStatistics(turns, minLifeSpanOfAnimals, maxLifeSpanOfAnimals, averageLifeSpanOfAnimals, currentStatistics);
            }
        }

        minLifeSpanOfAnimals = currentStatistics[0];
        averageLifeSpanOfAnimals = currentStatistics[1];
        maxLifeSpanOfAnimals = currentStatistics[2];

        averageLifeSpanOfAnimals /= animalsCount;

        Statistics statistics = new Statistics(minLifeSpanOfAnimals, maxLifeSpanOfAnimals, averageLifeSpanOfAnimals);

        return statistics;
    }

    private static List<Animal> CreateAnimals()
    {
        List<Animal> animals = new List<Animal>
        {
            new Bear { },
            new Wolf { },
            new Pigeon { },
            new Wolf { }
        };

        return animals;
    }

    private static List<Food> CreateFoods()
    {
        List<Food> foods = new List<Food>
        {
            new Beaver { },
            new Bison { },
            new Deer { },
            new Elk { },
            new Flower { },
            new Fruit { },
            new Grass { },
            new Hare { },
            new Moose { },
            new Rat { },
            new TallPlant { }
        };

        return foods;
    }

    /* TODO
    Implement the changing diets in a better way

    private static Animal ChangeDietIfAnimalCertainAgeAndType(Animal animal)
    {
        if (animal.AnimalType == "Carnivore" && animal.AnimalAge > 10)
        {
            animal.Diet.Add("Deer");
            animal.Diet.Add("Bison");
        }
        else if (animal.AnimalType == "Herbivore" && animal.AnimalAge > 7)
        {
            animal.Diet.Add("TallPlant");
        }
        else if (animal.AnimalType == "Omnivore" && animal.AnimalAge > 14)
        {
            animal.Diet.Add("Moose");
            animal.Diet.Add("Elk");
        }

        return animal;
    }
    */

    private static Food RandomFoodGenerator(List<Food> foods)
    {
        Random random = new Random();
        Food randomFood = (Food)foods.ElementAt(random.Next(foods.Count));

        return randomFood;
    }

    private static List<Food> AddAnimalsToFoods(List<Animal> animals, List<Food> foods)
    {
        foreach (var animal in animals)
        {
            foods.Add(animal);
        }

        return foods;
    }
}

public record Statistics(int MinLifeSpan, int MaxLifeSpan, int AvgLifeSpan);