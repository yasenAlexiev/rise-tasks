﻿namespace OOPNatureReserveSimulationSolution.Animals;

public class Pigeon : Animal
{
    public Pigeon() : base("pigeon", 4, 10, new HashSet<string> { "Grass", "Flower" }, "Herbivore")
    {
    }
}